import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'task.dart';


class TaskData extends ChangeNotifier{
  List<Task> _tasks = [
    Task(name: 'Finish flutter course'),
    Task(name: 'Go to dentist'),
    Task(name: 'Repair laptop'),
  ];

  int get taskCount {
    return _tasks.length;
  }

  UnmodifiableListView<Task> get tasks {
    return UnmodifiableListView(_tasks);
  }

  void addTask(Task newTask){
    _tasks.add(newTask);
    notifyListeners();
  }

  void updateTask(Task task){
    task.toggleDone();
    notifyListeners();
  }

  void removeTask(Task task){
    _tasks.remove(task);
    notifyListeners();
  }
}