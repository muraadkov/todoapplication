import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {
  final String name;
  final bool isChecked;
  final void Function(bool?) checkboxCallback;
  final void Function() longPress;
  TaskTile({required this.name, required this.isChecked, required this.checkboxCallback, required this.longPress});


  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        name,
        style: TextStyle(
          decoration: isChecked ? TextDecoration.lineThrough : null
        ),
      ),
      trailing: Checkbox(
        activeColor: Colors.lightBlueAccent,
        value: isChecked,
        onChanged: (newValue) {
          checkboxCallback(newValue);
        },
        // onChanged: toggleCheckboxState,
      ),
      onLongPress: () {
        longPress();
      },
    );
  }
}