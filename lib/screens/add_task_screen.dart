import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:todoey_flutter/models/task.dart';
import 'package:todoey_flutter/models/task_data.dart';

class AddTaskScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    String taskName = "";
    return Container(
      color: Color(0xff757575),
      child: Container(
        padding: EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0, bottom: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Add Task',
              style: TextStyle(
                color: Colors.lightBlueAccent,
                fontSize: 30.0,
                fontWeight: FontWeight.w500,
              ),
            ),
            TextField(
              textAlign: TextAlign.center,
              autofocus: true,
              decoration: InputDecoration(

              ),
              onChanged: (value) {
                taskName = value;
              },
            ),
            SizedBox(height: 20.0,),
            FlatButton(
                color: Colors.lightBlueAccent,
                minWidth: 100.0,
                child: Text(
                  'Add',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              onPressed: () {
                final task = Task(name: taskName);
                Provider.of<TaskData>(context).addTask(task);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );;
  }
}
